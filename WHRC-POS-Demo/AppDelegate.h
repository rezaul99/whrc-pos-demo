//
//  AppDelegate.h
//  WHRC POS
//
//  Created by Jijoty on 9/9/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterVC.h"
#import "CartVC.h"
#import "UISplitViewControllerCustom.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
