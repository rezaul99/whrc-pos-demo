//
//  HideShowMasterVCDelegate.h
//  WHRC POS
//
//  Created by Jijoty on 10/3/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HideShowMasterVCDelegate <NSObject>
@required
-(void)HideShowMasterVC:(bool)isHiden;
@end
