//
//  AppDelegate.m
//  WHRC POS
//
//  Created by Jijoty on 9/9/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
    [splitViewController setValue:[NSNumber numberWithFloat:700.0] forKey:@"_masterColumnWidth"];
    //splitViewController. = 1700;
    splitViewController.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    int count = splitViewController.viewControllers.count;
    
   /*UINavigationController *leftNavController = [splitViewController.viewControllers objectAtIndex:0];
    MasterVC *leftViewController = (MasterVC *)[leftNavController visibleViewController];
    CartVC *rightViewController = [splitViewController.viewControllers objectAtIndex:1];*/
    
   //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
   // MasterVC *leftViewController = [storyboard instantiateViewControllerWithIdentifier:@"MasterVC"];
    //CartVC *rightViewController = [storyboard instantiateViewControllerWithIdentifier:@"CartVC"];
    
  //  WHRCClass *firstMonster = [[leftViewController monsters] objectAtIndex:0];
   // [rightViewController setMonster:firstMonster];
   // leftViewController.delegate = rightViewController;
    
   /* UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    MFSideMenuContainerViewController *container = (MFSideMenuContainerViewController *)self.window.rootViewController;
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"navigationController"];
    UIViewController *leftSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"leftSideMenuViewController"];
    
    [container setLeftMenuViewController:leftSideMenuViewController];
    [container setCenterViewController:navigationController];*/
   
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
