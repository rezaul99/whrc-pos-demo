//
//  SQLHelper.h
//  whrc
//
//  Created by Rabeya on 12/4/13.
//  Copyright (c) 2013 Shahnawaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@interface SQLHelper : NSObject
{
    
}

-(int) Save:(NSString *)query;
-(BOOL *) Update:(NSString *)query;
-(BOOL *) Delete:(NSString *)query;
-(sqlite3_stmt *) Get:(NSString *)query;
//+(NSMutableArray *) GetDataList:(sqlite3_stmt *)statement:(NSString *) TableName;

@end
