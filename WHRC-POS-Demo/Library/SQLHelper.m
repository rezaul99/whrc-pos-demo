//
//  SQLHelper.m
//  whrc
//
//  Created by Rabeya on 12/4/13.
//  Copyright (c) 2013 Shahnawaz. All rights reserved.
//

#import "SQLHelper.h"
#import "Order.h"

@implementation SQLHelper

sqlite3 *_contactDB;
sqlite3 *database;
NSString *_databasePath;
NSString *databasePath;
NSString *databaseName = @"WHRCDB";

-(BOOL) CreateAllTable
{
    //if([self checkAndCreateDatabase])
    //{
            /***Order Table*/
    char *tableQuery = "CREATE TABLE IF NOT EXISTS Invoice (Id Integer PRIMARY KEY AUTOINCREMENT, UserId BIGINT, InvoiceType Integer, PurchaseDate VARCHAR, TotalAmount DECIMAL,Discount DECIMAL, PaidAmount DECIMAL, NetPayable DECIMAL, PackageDiscount DECIMAL, OrderStatus Integer, ClientRequestDiscountMsg VARCHAR, ItemNumber Integer)";
        if(![self CreateTable:tableQuery])
            return false;
            
        /*** OrderMembershipItem Table**/
    tableQuery = "CREATE TABLE IF NOT EXISTS OrderMembershipItem (Id Integer PRIMARY KEY AUTOINCREMENT, InvoiceId BIGINT, MembershipId BIGINT, Name VARCHAR, Period INT, DateJoined VARCHAR, DateValidTill VARCHAR, Description VARCHAR,Fee DECIMAL)";
        if(![self CreateTable:tableQuery])
            return false;
    
    tableQuery = "CREATE TABLE IF NOT EXISTS OrderClassItem (Id Integer PRIMARY KEY AUTOINCREMENT, InvoiceId BIGINT,SectionId BIGINT, SignupDate VARCHAR, NumberAttending Integer, Price DECIMAL, IsInWaitingList BOOL, ClassName VARCHAR, DayOfWeek VARCHAR, StartDate VARCHAR, StartTime VARCHAR, EndTime VARCHAR, Instructor VARCHAR, Location VARCHAR, NumberMeeting Integer)";
    if(![self CreateTable:tableQuery])
        return false;
    
    tableQuery = "CREATE TABLE IF NOT EXISTS User (Id Integer PRIMARY KEY AUTOINCREMENT, UserId BIGINT, FirstName VARCHAR, LastName VARCHAR, Email VARCHAR, Password VARCHAR)";
    if(![self CreateTable:tableQuery])
        return false;
    
    tableQuery = "CREATE TABLE IF NOT EXISTS OrderRentalItem (Id Integer PRIMARY KEY AUTOINCREMENT,InvoiceId BIGINT, RentalEquipmentId BIGINT, SerialNumber VARCHAR, VendorName VARCHAR, ModelName VARCHAR ,REStartDate VARCHAR, RPEndingDate VARCHAR, REPeriod INT, PretaxAmount DECIMAL, SalesTaxAmount DECIMAL, Total DECIMAL, DepositAmount DECIMAL)";
    if(![self CreateTable:tableQuery])
        return false;
    
    tableQuery = "CREATE TABLE IF NOT EXISTS OrderProductItem (Id Integer PRIMARY KEY AUTOINCREMENT, InvoiceId BIGINT,ProductId BIGINT, Name VARCHAR, Quantity INT, UnitPrice DECIMAL, Price DECIMAL,TaxAmount DECIMAL, Subtotal DECIMAL)";
    if(![self CreateTable:tableQuery])
        return false;
    
    sqlite3_close(_contactDB);
        return true;
    //}
    //sqlite3_close(_contactDB);
    //return false;
}
-(BOOL *) checkAndCreateDatabase
{
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(
                                                   NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    _databasePath = [[NSString alloc]
                     initWithString: [docsDir stringByAppendingPathComponent:
                                      databaseName]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: _databasePath ] == NO)
    {
        const char *dbpath = [_databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            if([self CreateAllTable])
                return true;
            else
                return false;
            //sqlite3_close(_contactDB);
        }
        else
        {
            return false;
        }
    }
    else
        return true;
}
-(BOOL *) CreateTable:(char *) query
{
    char *errMsg;
    const char *sql_stmt =query;
       // "CREATE TABLE IF NOT EXISTS CONTACTS (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, ADDRESS TEXT, PHONE TEXT)";
        
    if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
    {
        //sqlite3_close(_contactDB);
        return false;
    }
    else
    {
        //sqlite3_close(_contactDB);
        return true;
    }    
}
-(BOOL)OpenDatabase
{
    NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentsPaths objectAtIndex:0];
    databasePath = [documentDir stringByAppendingPathComponent:databaseName];
    if([self checkAndCreateDatabase])
    {    
        if(sqlite3_open([databasePath UTF8String],&database) == SQLITE_OK)
        {
            return true;
        }
    }
    return false;
}
-(int) Save:(NSString *)query
{
    int Id = 0;
    if([self OpenDatabase])
    {        
        sqlite3_stmt    *statement;
        const char *dbpath = [_databasePath UTF8String];
    
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            NSString *insertSQL = query;
      //  NSString *insertSQL = [NSString stringWithFormat:
                             //  @"INSERT INTO CONTACTS (name, address, phone) //VALUES (\"%@\", \"%@\", \"%@\")",
                              // @"a", @"a",@"a"];
        
            const char *insert_stmt = [insertSQL UTF8String];
            sqlite3_prepare_v2(database, insert_stmt,
                           -1, &statement, NULL);
        
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                Id = sqlite3_last_insert_rowid(database);
                sqlite3_finalize(statement);
                sqlite3_close(database);
                return Id;
            }
            else
            {
                sqlite3_finalize(statement);
                sqlite3_close(database);
                return Id;
            }        
        }
    }
    return Id;
}

-(BOOL *) Update:(NSString *)query
{
    if([self OpenDatabase])
    {
        sqlite3_stmt    *statement;
        const char *dbpath = [_databasePath UTF8String];
    
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
       // NSString *insertSQL = [NSString stringWithFormat:
                             //  @"UPDATE  CONTACTS SET name=\"%@\", address=\"%@\", phone=\"%@\" WHERE name=\"%@\"",
                            //   @"c", @"c",@"c",@"a"];        
        
            NSString *insertSQL = query;
            const char *insert_stmt = [insertSQL UTF8String];
            if(sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL)== SQLITE_OK)
            {
        
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    return true;
                }
                else
                {
                    return false;;
                }
                sqlite3_finalize(statement);
            }
            sqlite3_close(database);
        }
    }
    return false;
}

-(BOOL *) Delete:(NSString *)query
{
    sqlite3_stmt    *statement;
    const char *dbpath = [_databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        
       // NSString *insertSQL = [NSString stringWithFormat:
                            //   @"DELETE FROM  CONTACTS  WHERE name=\"%@\"",
                            //   @"b"];
        
         NSString *insertSQL = query;
        
        const char *insert_stmt = [insertSQL UTF8String];
        if(sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL)== SQLITE_OK)
        {
        
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }

    return false;
}

-(sqlite3_stmt *) Get:(NSString *)query
{
    //const char *dbpath = [_databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    /*sqlite3 *database;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:@"WHRCDB"];
    const char* dbPath=[databasePath UTF8String];*/
    if([self OpenDatabase])
    {
        
        //NSString *querySQL = @"SELECT * FROM Invoice";//
        
        if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            //sqlite3_finalize(statement);
            sqlite3_close(database);

            return statement;
            /*if(sqlite3_step(statement) == SQLITE_ROW)
            {
                sqlite3_finalize(statement);
                sqlite3_close(database);

                return  statement;
            }
            else
            {
                return nil;
            } */           
        }
        else
        {
            NSLog(@"Error :",sqlite3_errmsg(database));
        }
        
       // sqlite3_close(_contactDB);
    }

    return nil;
}

-(NSMutableArray *) GetDataList:(sqlite3_stmt *)statement:(NSString *) TableName
{
    if([TableName isEqualToString:@"ClassInvoiceItem"])
    {
        while(sqlite3_step(statement) == SQLITE_ROW)
       {
           NSString *addressField = [[NSString alloc]
                                  initWithUTF8String:
                                  (const char *) sqlite3_column_text(
                                                                     statement, 0)];
           NSLog(@"Tilte: %@", addressField);
        
           NSString *phoneField = [[NSString alloc]
                                initWithUTF8String:(const char *)
                                sqlite3_column_text(statement, 1)];
        // _phone.text = phoneField;
           NSLog(@"Tilte: %@", phoneField);
            NSLog(@"Tilte: %@", @" found");
       }
    
    }
    return nil;
}

@end
