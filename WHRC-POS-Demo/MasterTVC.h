//
//  MasterTVC.h
//  WHRC POS
//
//  Created by Jijoty on 9/10/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "OrderManager.h"
#import "UserManager.h"
#import "MiscellaniesManager.h"
#import "WHRCClass.h"
#import "ClassTVCell.h"
#import "WebServiceHelper.h"
#import "ItemSelectionDelegate.h"

@interface MasterTVC : UITableViewController
{
    Order *orderObj;
    NSMutableArray * listofObjects;
    MiscellaniesManager *misManager ;
}
//@property (nonatomic, assign) id<ItemSelectionDelegate> delegate;
@end
