//
//  DiscountVC.m
//  WHRC POS
//
//  Created by Jijoty on 11/3/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import "DiscountVC.h"

@interface DiscountVC ()

@end

@implementation DiscountVC
NSString *mode;
long itemId ;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    listofObjects = [NSMutableArray array];
    listofObjectsName = [NSMutableArray array];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    double amount = [[defaults objectForKey:@"ItemQuantity"] integerValue];
    
    self.lblItemName.text = @"Discount";
    self.tfQuantity.text = [NSString stringWithFormat:@"%.2f%%",amount];
    
    [self.rbPackage setSelected:YES];
    [self.rbPercentage setSelected:NO];
    [self.rbOther setSelected:NO];
    [self.rbPackage  setImage:[UIImage imageNamed:@"RBOn.png"] forState:UIControlStateSelected];
    [self.rbPercentage  setImage:[UIImage imageNamed:@"RBOff.png"] forState:UIControlStateNormal];
    [self.rbOther  setImage:[UIImage imageNamed:@"RBOff.png"] forState:UIControlStateNormal];
    
    webServiceHelper= [[WebServiceHelper alloc]init];
    listofObjects = [webServiceHelper GetAll:@"Discount"];
    NSString *name = @"";
    mode = @"";
    double discountAmount = 0;
    for (Discount *disObj in listofObjects)
    {
        if([disObj.Type isEqualToString:@"Package"])
        {
            name = [NSString stringWithFormat:@"%@|%.2f",disObj.Name,disObj.Discount];
            discountAmount = disObj.Discount;
            mode = disObj.Mode;
            [listofObjectsName addObject:name];
        }
    }
    self.lblDiscountAmount.text = @"Discount Amount($)";
    self.tfQuantity.text = [NSString stringWithFormat:@"$%.2f",discountAmount];
    self.tfSubType.text = name;
    self.textmenuSubType = [[UIDropDownMenu alloc] initWithIdentifier:@"textmenuSubType"];
    self.textmenuSubType.ScaleToFitParent = TRUE;
    self.textmenuSubType.titleArray = listofObjectsName;
    self.textmenuSubType.valueArray = listofObjectsName;
    [self.textmenuSubType makeMenu:self.tfSubType targetView:self.view];
    self.textmenuSubType.delegate = self;
}

- (IBAction)btnClickApplyDiscount:(id)sender{
    NSString *strDiscountAmount = [self.tfQuantity.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    double discountAmount = [strDiscountAmount doubleValue];
    
    NSString *discountType=@"";
    long subTypeId = 0;
    if([self.rbPackage isSelected]==YES)
       discountType = @"Package";
    else if([self.rbPercentage isSelected]==YES)
        discountType = @"Percentage";
    else if([self.rbOther isSelected]==YES)
        discountType = @"Other";
    
    NSArray* foo = [self.tfSubType.text componentsSeparatedByString: @"|"];
    NSString *strName = [foo objectAtIndex: 0];
    for (Discount *disObj in listofObjects)
    {
        if([disObj.Name isEqualToString:strName])
        {
            subTypeId = disObj.RecordId;
            break;
        }
    }
    
    
    UISplitViewController *svController = (UISplitViewController*)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    CartVC *cVC= [svController.viewControllers objectAtIndex:1];
    self.delegateQuantity = cVC;
    if (_delegateQuantity)
    {
        [_delegateQuantity UpdateDiscount:discountAmount:discountType:subTypeId];
    }
}
- (IBAction)btnClickClearDiscount:(id)sender {
    
    UISplitViewController *svController = (UISplitViewController*)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    CartVC *cVC= [svController.viewControllers objectAtIndex:1];
    self.delegateQuantity = cVC;
    if (_delegateQuantity)
    {
        [_delegateQuantity UpdateDiscount:0:0:0];
    }
}

- (IBAction)rbGroupClicked:(UIButton *)button
{
    switch (button.tag)
    {
        case 1:
            if([self.rbPackage isSelected]==YES)
            {
                [self.rbPackage setSelected:YES];
            }
            else
            {
                listofObjectsName = [NSMutableArray array];
                NSString *name;
                double discountAmount = 0;
                for (Discount *disObj in listofObjects)
                {
                    if([disObj.Type isEqualToString:@"Package"])
                    {
                        name = [NSString stringWithFormat:@"%@|%.2f",disObj.Name,disObj.Discount];
                        discountAmount = disObj.Discount;
                        mode = disObj.Mode;
                        [listofObjectsName addObject:name];
                    }
                }
                self.lblDiscountAmount.text = @"Discount Amount($)";
                self.tfQuantity.text = [NSString stringWithFormat:@"$%.2f",discountAmount];
                self.tfSubType.text = name;
                
                self.textmenuSubType.ScaleToFitParent = TRUE;
                self.textmenuSubType.titleArray = listofObjectsName;
                self.textmenuSubType.valueArray = listofObjectsName;
                [self.textmenuSubType makeMenu:self.tfSubType targetView:self.view];
                self.textmenuSubType.delegate = self;
                
                [self.rbPackage setSelected:YES];
                [self.rbPercentage setSelected:NO];
                [self.rbOther setSelected:NO];
                    
                [self.rbPackage setImage:[UIImage imageNamed:@"RBOn.png"] forState:UIControlStateSelected];
                [self.rbPercentage setImage:[UIImage imageNamed:@"RBOff.png"] forState:UIControlStateNormal];
                [self.rbOther setImage:[UIImage imageNamed:@"RBOff.png"] forState:UIControlStateNormal];
            }
            break;
        case 2:
            if([self.rbPercentage isSelected]==YES)
            {
                [self.rbPercentage setSelected:YES];
            }
            else
            {
                listofObjectsName = [NSMutableArray array];
                NSString *name=@"";
                double discountAmount = 0;
                for (Discount *disObj in listofObjects)
                {
                    if([disObj.Type isEqualToString:@"Percentage"])
                    {
                        name = [NSString stringWithFormat:@"%@|%.2f",disObj.Name,disObj.Discount];
                        discountAmount = disObj.Discount;
                        mode = disObj.Mode;
                        [listofObjectsName addObject:name];
                    }
                }
                self.lblDiscountAmount.text = @"Discount Amount(%)";
                double totalDue = [[[NSUserDefaults standardUserDefaults] objectForKey:@"TotalDue"] doubleValue];
                //double discountPercentage = [strDiscount doubleValue];
                double discount = (totalDue * discountAmount)/100;
                self.tfQuantity.text = [NSString stringWithFormat:@"$%.2f",discount];
                self.tfSubType.text = name;
                
                self.textmenuSubType.ScaleToFitParent = TRUE;
                self.textmenuSubType.titleArray = listofObjectsName;
                self.textmenuSubType.valueArray = listofObjectsName;
                [self.textmenuSubType makeMenu:self.tfSubType targetView:self.view];
                self.textmenuSubType.delegate = self;
                    
                [self.rbPackage setSelected:NO];
                [self.rbPercentage setSelected:YES];
                [self.rbOther setSelected:NO];
                    
                [self.rbPackage setImage:[UIImage imageNamed:@"RBOff.png"] forState:UIControlStateNormal];
                [self.rbPercentage setImage:[UIImage imageNamed:@"RBOn.png"] forState:UIControlStateSelected];
                [self.rbOther setImage:[UIImage imageNamed:@"RBOff.png"] forState:UIControlStateNormal];
            }
            break;
        case 3:
            if([self.rbOther isSelected]==YES)
            {
                [self.rbOther setSelected:YES];
            }
            else
            {
                listofObjectsName = [NSMutableArray array];
                NSString *name=@"";
               double discountAmount = 0;
                for (Discount *disObj in listofObjects)
                {
                    if([disObj.Type isEqualToString:@"Other"])
                    {
                        name = [NSString stringWithFormat:@"%@|%.2f",disObj.Name,disObj.Discount];
                        mode = disObj.Mode;
                        discountAmount = disObj.Discount;
                        [listofObjectsName addObject:name];
                    }
                }
                if([mode isEqualToString:@"Amount"])
                {
                    self.lblDiscountAmount.text = @"Discount Amount($)";
                    self.tfQuantity.text = [NSString stringWithFormat:@"$%.2f",discountAmount];
                }
                else
                {
                    self.lblDiscountAmount.text = @"Discount Amount(%)";
                    double totalDue = [[[NSUserDefaults standardUserDefaults] objectForKey:@"TotalDue"] doubleValue];
                    //double discountPercentage = [strDiscount doubleValue];
                    double discount = (totalDue * discountAmount)/100;
                    self.tfQuantity.text = [NSString stringWithFormat:@"$%.2f",discount];
                }
                
                self.tfSubType.text = name;
                
                self.textmenuSubType.ScaleToFitParent = TRUE;
                self.textmenuSubType.titleArray = listofObjectsName;
                self.textmenuSubType.valueArray = listofObjectsName;
                [self.textmenuSubType makeMenu:self.tfSubType targetView:self.view];
                self.textmenuSubType.delegate = self;
                    
                [self.rbPackage setSelected:NO];
                [self.rbPercentage setSelected:NO];
                [self.rbOther setSelected:YES];
                
                [self.rbPackage setImage:[UIImage imageNamed:@"RBOff.png"] forState:UIControlStateNormal];
                [self.rbPercentage setImage:[UIImage imageNamed:@"RBOff.png"] forState:UIControlStateNormal];
                [self.rbOther setImage:[UIImage imageNamed:@"RBOn.png"] forState:UIControlStateSelected];
            }
            break;
        default:
            break;
    }
}
- (void) DropDownMenuDidChange:(NSString *)identifier :(NSString *)ReturnValue
{
    NSArray* foo = [ReturnValue componentsSeparatedByString: @"|"];
    NSString *strName = [foo objectAtIndex: 0];
    NSString *strDiscount = [foo objectAtIndex: 1];
    self.tfSubType.text = ReturnValue;
    if([mode isEqualToString:@"Amount"])
    {
       self.tfQuantity.text = [NSString stringWithFormat:@"$%@",strDiscount];
    }
    else
    {
        double totalDue = [[[NSUserDefaults standardUserDefaults] objectForKey:@"TotalDue"] doubleValue];
        double discountPercentage = [strDiscount doubleValue];
        double discount = (totalDue * discountPercentage)/100;
        self.tfQuantity.text = [NSString stringWithFormat:@"$%.2f",discount];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
