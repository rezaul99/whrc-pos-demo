//
//  MiscellaniesManager.m
//  WHRC
//
//  Created by Jijoty on 5/16/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import "MiscellaniesManager.h"

@implementation MiscellaniesManager

- (UIColor *)ColorFromHexString:(NSString *)hexString:(CGFloat) opacity
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:opacity];
}
- (BOOL) validateEmail: (NSString *) candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}
- (BOOL)IsInternetConnectionAvaiable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    //Reachability* hostReachable = [Reachability reachabilityWithHostName:@"www.apple.com"];
    /*if(networkStatus != NotReachable)
    {
      // Reachability *hostReachable = [Reachability reachabilityWithHostName:@"www.momscradle.com"];
        Reachability *hostReachable = [Reachability reachabilityWithAddress:@"173.11.76.130"];
        NetworkStatus internetStatus = [hostReachable currentReachabilityStatus];
        
        return internetStatus!=NotReachable;
    }*/
    return networkStatus !=NotReachable;
  
}
@end
