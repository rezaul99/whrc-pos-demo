//
//  MiscellaniesManager.h
//  WHRC
//
//  Created by Jijoty on 5/16/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface MiscellaniesManager : NSObject
- (UIColor *)ColorFromHexString:(NSString *)hexString:(CGFloat) opacity;
- (BOOL) validateEmail: (NSString *) candidate;
- (BOOL)IsInternetConnectionAvaiable;
@end
