//
//  UserManager.m
//  whrc
//
//  Created by Jijoty on 1/27/14.
//  Copyright (c) 2014 Shahnawaz. All rights reserved.
//

#import "UserManager.h"
#import "User.h"
@implementation UserManager

-(int) Save:(User *) userObj
{
    NSString *query =  [NSString stringWithFormat:@"INSERT INTO User (UserId , FirstName, LastName , Email ,Password) VALUES (\"%ld\", \"%@\", \"%@\", \"%@\",\"%@\")",
     userObj.UserId,userObj.FirstName,userObj.LastName,userObj.Email,userObj.Password];
    
    SQLHelper *sqlHelper = [[SQLHelper alloc]init];
    userObj.Id = [sqlHelper Save:query];
    return userObj.Id;
}
-(User*) Get:(int)Id
{
    NSString *query = [NSString stringWithFormat:@"Select * FROM  User  WHERE ID=\"%d\"", Id];
    SQLHelper *sqlHelper = [[SQLHelper alloc]init];
    return [self GetSingle:[sqlHelper Get: query]];
}

-(User *) GetSingle:(sqlite3_stmt *)statement
{
    User *userObj = nil;
    while (sqlite3_step(statement) == SQLITE_ROW)
    {
        int uniqueId = sqlite3_column_int(statement, 0);
        long userId = sqlite3_column_int64(statement, 1);
        char *firstName = (char *)sqlite3_column_text(statement, 2);
        char *lastName = (char *)sqlite3_column_text(statement, 3);
        char *email = (char *)sqlite3_column_text(statement, 4);
        char *password = (char *)sqlite3_column_text(statement, 5);
        
        userObj = [[User alloc] init];
        
        userObj.Id = [NSNumber numberWithInt:uniqueId];
        userObj.UserId = [NSNumber numberWithLong:userId];
        userObj.FirstName = [[NSString alloc] initWithUTF8String:firstName];
        userObj.LastName = [[NSString alloc] initWithUTF8String:lastName];
        userObj.Email = [[NSString alloc] initWithUTF8String:email];
        userObj.Password = [[NSString alloc] initWithUTF8String:password];
    }
    sqlite3_finalize(statement);
    
    return userObj;
}
-(NSMutableArray *) GetList:(sqlite3_stmt *)statement
{
    User *userObj = nil;
    NSMutableArray *listofObjects = [NSMutableArray array];
    
    while (sqlite3_step(statement) == SQLITE_ROW)
    {
        int uniqueId = sqlite3_column_int(statement, 0);
        long userId = sqlite3_column_int64(statement, 1);
        char *firstName = (char *)sqlite3_column_text(statement, 2);
        char *lastName = (char *)sqlite3_column_text(statement, 3);
        char *email = (char *)sqlite3_column_text(statement, 4);
        char *password = (char *)sqlite3_column_text(statement, 5);
        
        userObj = [[User alloc] init];
        
        userObj.Id = [NSNumber numberWithInt:uniqueId];
        userObj.UserId = [NSNumber numberWithLong:userId];
        userObj.FirstName = [[NSString alloc] initWithUTF8String:firstName];
        userObj.LastName = [[NSString alloc] initWithUTF8String:lastName];
        userObj.Email = [[NSString alloc] initWithUTF8String:email];
        userObj.Password = [[NSString alloc] initWithUTF8String:password];
        
        [listofObjects addObject:userObj];
    }
    sqlite3_finalize(statement);
    
    return listofObjects;
}

@end
