//
//  UserManager.h
//  whrc
//
//  Created by Jijoty on 1/27/14.
//  Copyright (c) 2014 Shahnawaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import"User.h"
#import "SQLHelper.h"

@interface UserManager : NSObject
-(int) Save:(User *) userObj;
-(BOOL *) Update:(User *)userObj;
-(User *) Get:(int)Id;
-(User *) GetSingle:(sqlite3_stmt *)statement;
-(NSMutableArray *) GetList:(sqlite3_stmt *)statement;

@end
