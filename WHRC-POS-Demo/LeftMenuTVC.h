//
//  LeftMenuTVC.h
//  WHRC POS
//
//  Created by Jijoty on 9/23/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuTVC : UITableViewController
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
@end
