//
//  DeleteVC.m
//  WHRC POS
//
//  Created by Jijoty on 9/29/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import "DeleteVC.h"

@interface DeleteVC ()

@end

@implementation DeleteVC
NSString *ItemType;
NSString *ItemName;
long itemId ;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    itemId = [[defaults objectForKey:@"ItemId"] longValue];
    ItemName = [defaults objectForKey:@"ItemName"];
    ItemType = [defaults objectForKey:@"ItemType"];
    int ItemQuantity = [[defaults objectForKey:@"ItemQuantity"] integerValue];
    
    self.lblItemName.text = ItemName;
    self.lblItemQty.text = [NSString stringWithFormat:@"%d",ItemQuantity];    
}

- (IBAction)btnClickSubtract:(id)sender {
    int quantity = [self.lblItemQty.text intValue];
    
    UISplitViewController *svController = (UISplitViewController*)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    CartVC *cVC= [svController.viewControllers objectAtIndex:1];
    if([ItemType isEqualToString:@"Product"] && quantity > 1)
    {
        self.lblItemQty.text = [NSString stringWithFormat:@"%d",quantity-1];
        
        self.delegateQuantity = cVC;
        if (_delegateQuantity)
        {
            [_delegateQuantity UpdateQuantity:quantity-1];
        }
    }
    else if([ItemType isEqualToString:@"Class"] && quantity > 1)
    {
        self.lblItemQty.text = [NSString stringWithFormat:@"%d",quantity-1];
        self.delegateQuantity = cVC;
        if (_delegateQuantity)
        {
            [_delegateQuantity UpdateQuantity:quantity-1];
        }
    }
}
- (IBAction)btnClickAdd:(id)sender {
    int quantity = [self.lblItemQty.text intValue];
    
    UISplitViewController *svController = (UISplitViewController*)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    CartVC *cVC= [svController.viewControllers objectAtIndex:1];
    if([ItemType isEqualToString:@"Product"] && quantity < 10)
    {
        self.lblItemQty.text = [NSString stringWithFormat:@"%d",quantity+1];
        self.delegateQuantity = cVC;
        if (_delegateQuantity)
        {
            [_delegateQuantity UpdateQuantity:quantity+1];
        }
    }
    else if([ItemType isEqualToString:@"Class"] && quantity < 2)
    {
        if(![ItemName isEqualToString:@"Infant CPR"])
        {
            self.lblItemQty.text = [NSString stringWithFormat:@"%d",quantity+1];
            self.delegateQuantity = cVC;
            if (_delegateQuantity)
            {
                [_delegateQuantity UpdateQuantity:quantity+1];
            }
        }
    }
}
- (IBAction)btnClickDeleteItem:(id)sender {
    UISplitViewController *svController = (UISplitViewController*)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    CartVC *cVC= [svController.viewControllers objectAtIndex:1];
    self.delegateQuantity = cVC;
    if (_delegateQuantity)
    {
        [_delegateQuantity UpdateQuantity:0];
    }
    [self dismissViewControllerAnimated:true completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
