//
//  UserTVCell.m
//  WHRC POS
//
//  Created by Jijoty on 9/29/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import "UserTVCell.h"

@implementation UserTVCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
