//
//  SubCategoryCell.h
//  WHRC POS
//
//  Created by Jijoty on 9/25/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubCategoryCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblCategoryName;
@property (strong, nonatomic) IBOutlet UILabel *lblYear;
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblPackage;
@property (weak, nonatomic) IBOutlet UILabel *lblYearHeader;
@property (weak, nonatomic) IBOutlet UIImageView *imgVCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceHeader;
@end
