//
//  UserTVCell.h
//  WHRC POS
//
//  Created by Jijoty on 9/29/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserTVCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgViewUser;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@end
