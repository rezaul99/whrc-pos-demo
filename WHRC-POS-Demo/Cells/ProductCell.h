//
//  ProductCell.h
//  whrc
//
//  Created by Jijoty on 12/10/13.
//  Copyright (c) 2013 Shahnawaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblProductCategory;
@property (strong, nonatomic) IBOutlet UILabel *lblSalePrice;
@property (strong, nonatomic) IBOutlet UILabel *lblQuantityInStock;
@property (strong, nonatomic) IBOutlet UILabel *lblTaxable;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblItemName;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToCart;
@property (weak, nonatomic) IBOutlet UILabel *lblInStock;
@end
