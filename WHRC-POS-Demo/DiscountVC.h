//
//  DiscountVC.h
//  WHRC POS
//
//  Created by Jijoty on 11/3/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuantityAndDeleteItemDelegate.h"
#import "CartVC.h"
#import "AppDelegate.h"
#import "UIDropDownMenu.h"

@interface DiscountVC : UIViewController<UIDropDownMenuDelegate>
{
    NSMutableArray * listofObjectsName;
    NSMutableArray * listofObjectsValue;
    NSMutableArray * listofObjects;
    WebServiceHelper *webServiceHelper;
}
@property (nonatomic, assign) id<QuantityAndDeleteItemDelegate> delegateQuantity;
@property (strong, nonatomic) UIDropDownMenu *textmenuSubType;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscountAmount;

@property (weak, nonatomic) IBOutlet UILabel *lblItemName;
@property (weak, nonatomic) IBOutlet UITextField *tfSubType;
@property (weak, nonatomic) IBOutlet UITextField *tfQuantity;

@property (weak, nonatomic) IBOutlet UIButton *rbPackage;
@property (weak, nonatomic) IBOutlet UIButton *rbPercentage;
@property (weak, nonatomic) IBOutlet UIButton *rbOther;
- (IBAction)btnClickClearDiscount:(id)sender;

- (IBAction)rbGroupClicked:(UIButton *)sender;
- (IBAction)btnClickApplyDiscount:(id)sender;
@end
