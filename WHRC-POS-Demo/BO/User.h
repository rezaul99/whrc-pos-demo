//
//  User.h
//  whrc
//
//  Created by Jijoty on 1/21/14.
//  Copyright (c) 2014 Shahnawaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property(nonatomic) BOOL *IsUserExist;
@property (nonatomic) int Id;
@property (nonatomic) long UserId;
@property (nonatomic, retain) NSString* Result;
@property (nonatomic, retain) NSString* FirstName;
@property (nonatomic, retain) NSString* LastName;
@property (nonatomic, retain) NSString* Email;
@property (nonatomic, retain) NSString* Password;
@property (nonatomic, retain) NSString* UserType;
@property (nonatomic, retain) NSString* Address1;
@property (nonatomic, retain) NSString* Ad1State;
@property (nonatomic, retain) NSString* Ad1Zip;
@property (nonatomic, retain) NSString* Ad1City;
@property (nonatomic, retain) NSString* DateOfBirth;
@property (nonatomic, retain) NSString* Insurance;
@property (nonatomic, retain) NSString* Hospital;
@property (nonatomic, retain) NSString* Phone;
@property (nonatomic, retain) NSString* PhoneType;
@property (nonatomic, retain) NSString* ImageBig;
@property (nonatomic, retain) NSString* ImageSmall;
@property (nonatomic, retain) NSString* CreateDate;
@end
