//
//  CreditCard.h
//  WHRC POS
//
//  Created by Jijoty on 11/12/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreditCard : NSObject

@property (nonatomic, retain) NSString* LoginId;
@property (nonatomic, retain) NSString* Password;
@property (nonatomic, retain) NSString* MarchentLoginId;
@property (nonatomic, retain) NSString* MarchentTransactionKey;
@property (nonatomic, retain) NSString* ImplementationMode;
@property (nonatomic, retain) NSString* MarchentPassword;
@property int GatewayTypeId;
@end
