//
//  Product.h
//  whrc
//
//  Created by Jijoty on 12/10/13.
//  Copyright (c) 2013 Shahnawaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (nonatomic) long RecordId;
@property (nonatomic, retain) NSString* ProductCategory;
@property (nonatomic, retain) NSString* ItemName;
@property (nonatomic, retain) NSString* Description;
@property (nonatomic) double SalePrice;
@property (nonatomic) double TaxRate;
@property (nonatomic) int QuantityInStock;
@property (nonatomic, retain) NSString* StatusofProduct;
@property (nonatomic, retain) NSString* Taxable;
@property (nonatomic, retain) NSString* ProductImageUrl;
@property (nonatomic, retain) NSString* ProductImageUrlBig;

@end
