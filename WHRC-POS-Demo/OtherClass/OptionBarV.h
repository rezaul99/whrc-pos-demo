//
//  OptionBarV.h
//  WHRC
//
//  Created by Jijoty on 9/2/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionBarV : UIView
@property(strong, nonatomic) IBOutlet UITextField *tfSearch;
@property (strong, nonatomic) IBOutlet UIButton *idBtnSelectCategory;

@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UIButton *rb1;
@property (weak, nonatomic) IBOutlet UIButton *rb2;
@property (weak, nonatomic) IBOutlet UIButton *rb3;

@end
