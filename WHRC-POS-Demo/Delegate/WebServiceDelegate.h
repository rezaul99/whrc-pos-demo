//
//  WebServiceDelegate.h
//  WHRC POS
//
//  Created by Jijoty on 11/18/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WebServiceDelegate <NSObject>
@required
-(void)GetDataSucceed:(NSMutableArray *)dataList:(NSString *)toolName;
-(void)GetDataFailed:(NSString *)failMessage;
@end
