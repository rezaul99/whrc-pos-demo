//
//  DeleteVC.h
//  WHRC POS
//
//  Created by Jijoty on 9/29/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuantityAndDeleteItemDelegate.h"
#import "CartVC.h"
#import "AppDelegate.h"

@interface DeleteVC : UIViewController
{
    
}
@property (nonatomic, assign) id<QuantityAndDeleteItemDelegate> delegateQuantity;

@property (weak, nonatomic) IBOutlet UILabel *lblItemName;
@property (weak, nonatomic) IBOutlet UILabel *lblItemQty;
@property (weak, nonatomic) IBOutlet UISplitViewController *svController;

- (IBAction)btnClickSubtract:(id)sender;
- (IBAction)btnClickDeleteItem:(id)sender;
- (IBAction)btnClickAdd:(id)sender;
@end
