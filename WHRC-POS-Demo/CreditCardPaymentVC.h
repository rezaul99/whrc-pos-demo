//
//  CreditCardPaymentVC.h
//  WHRC POS
//
//  Created by Jijoty on 10/3/14.
//  Copyright (c) 2014 Jijoty. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "OrderManager.h"
#import "WebServiceHelper.h"
#import "User.h"
#import "UserManager.h"
#import "HeaderV.h"
#import "MiscellaniesManager.h"
#import "AuthNet.h"

@interface CreditCardPaymentVC : UIViewController<UITextFieldDelegate,UIScrollViewDelegate,AuthNetDelegate>
{
    Order *orderObj;
    NSMutableArray * listofObjects;
    IBOutlet UIScrollView *scroller;
}
@property (nonatomic) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) NSString *sessionToken;

@property(nonatomic) double PackageDiscount;
@property(nonatomic) double MedicalAmount;
@property(strong) NSString * MedicalNumber;

@property (weak, nonatomic) IBOutlet HeaderV *vHeader;

@property (weak, nonatomic) IBOutlet UILabel *lblCheckout;

@property (weak, nonatomic) IBOutlet UILabel *lblSubtotal;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPackageDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalDue;
@property (weak, nonatomic) IBOutlet UITextField *tfCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfExpirationDate;
@property (weak, nonatomic) IBOutlet UITextField *tfSecurityCode;
@property (weak, nonatomic) IBOutlet UITextField *tfFirstName;
@property (weak, nonatomic) IBOutlet UITextField *tfLastName;
@property (weak, nonatomic) IBOutlet UITextField *tfAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfCity;
@property (weak, nonatomic) IBOutlet UITextField *tfZip;
@property (weak, nonatomic) IBOutlet UITextField *tfState;

@property (weak, nonatomic) IBOutlet UIView *vMainUI;
@property (weak, nonatomic) IBOutlet UIButton *idChkBtnSameAsProfile;
@property (weak, nonatomic) IBOutlet UIView *insiderView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

-(IBAction) ChkBtnSameAsProfile:(id)sender;
-(IBAction) Pay : (id)sender;
- (IBAction)BtnSecurityCodeInfoClick:(id)sender;
- (IBAction)btnClickBack:(id)sender;
@end
