WHRC POS Demo
==================

This is a iOS(iPad) project template of SAAS based WHRC POS.

It is a POS (point of Service) type application. Customer can use their Credit Card for payment. 
Epson TM-T88V intellegent printer is integrated for printing order receipt, Dymo label printer 450 is integrated for printing product’s bar code, Socket Mobile bar code scanner is integrated for scanning product’s bar code for add products to cart, Magtek secure Credit card reader is integrated for secure payment, Cash registers is integrated for managing cash service with this POS.


It should look something like this:

![WHRC-POS-Demo](login.png)

![WHRC-POS-Demo](home.png)

![WHRC-POS-Demo](payment.png)
